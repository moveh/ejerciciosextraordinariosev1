<?php
    $monedas = ['Euro' => 1, 'Libra' => 1.15, 'Dolar' => 0.85];
    if(isset($_POST['enviar'])){
        $origen = $_POST['origen'];
        $destino = $_POST['destino'];
        $dinero = $_POST['cantidad'];
        $convertido = round($dinero*$monedas[$origen]/$monedas[$destino], 2);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2</title>
</head>
<body>
    <form action="" method="post">
        <?php if(isset($_POST['cantidad'])):?>
            <input type="number" name="cantidad" id="cantidad" value="<?=$dinero?>" >
        <?php else :?>
            <input type="number" name="cantidad" id="cantidad" value='0' >
        <?php endif ;?>
        <br><br>
        <select name="origen" id="origen">
            <?php foreach ($monedas as $moneda => $valor) :?>
            <?php if($moneda == $origen):?>
                <option value="<?=$moneda?>" selected><?=$moneda?></option>
            <?php else :?>
               <option value="<?=$moneda?>"><?=$moneda?></option>
            <?php endif ;?>
            <?php endforeach ;?> 
        </select>
        <select name="destino" id="destino">
        <?php foreach ($monedas as $moneda => $valor) :?>
            <?php if($moneda == $destino):?>
                <option value="<?=$moneda?>" selected><?=$moneda?></option>
            <?php else :?>
               <option value="<?=$moneda?>"><?=$moneda?></option>
            <?php endif ;?>
            <?php endforeach ;?> 
        </select>
        <input type="submit" name="enviar" value="enviar">
    </form>
    <br><br>
    <?php if(isset($_POST['enviar'])) :?>
    <?= $dinero.' '.$origen.' son '.$convertido.' '.$destino ?>
    <?php endif ;?>
</body>
</html>