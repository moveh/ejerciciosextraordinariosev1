<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cinco</title>
</head>
<body>
    <?php
    $numero=123;
    $aux=$numero;

    $suma = 0;
    $producto = 1;
    
    while ($aux>0)
    {
        $digito = $aux%10;
        $aux = floor($aux/10);
        $suma += $digito;
        $producto *= $digito;

    }
    
    echo $suma == $producto ? "Los números coinciden" : "Los números no coinciden" ;

    ?>
</body>
</html>