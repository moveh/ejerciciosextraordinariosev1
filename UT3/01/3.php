<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tres</title>
</head>
<body>
    <?php
        $valor = 1221;
        $numinv = 0;
        $cociente = $valor;
        while ( $cociente != 0)
        {
            $resto = $cociente % 10;
            $numinv = $numinv * 10 + $resto;
            $cociente = floor($cociente / 10);
        }
        if ( $valor == $numinv )
            echo "El número $valor es capicúa <br />";
        else
            echo "El número $valor NO es capicúa <br />";
    ?>
</body>
</html>