<?php

class Coche{

    private $matricula;
    private $velocidad;

    public function __construct($matricula, $velocidad)
    {
        $this->matricula = $matricula;
        $this->velocidad = $velocidad;
    }

    public function acelera($kmh){
        $vel = $this->velocidad + $kmh;

        if($vel > 120){
            $vel = 120;
        }

        $this->velocidad = $vel;
    }

    public function frena($kmh){
        $vel = $this->velocidad - $kmh;
        if($vel < 0){
            $vel = 0;
        }
        $this->velocidad = $vel;
    }

    public function __toString()
    {
        return 'La velocidad del coche con matricula '.$this->matricula.' es de: '.$this->velocidad.' km/h';
    }
   
}

?>