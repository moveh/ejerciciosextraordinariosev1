<?php
    require_once 'circulo.php';
    $radio1 = 10;
    $circulo = new Circulo($radio1);
    $circulo = 20;
    echo 'El radio es: '.$circulo.'<br>';

    require_once 'coche.php';
    $acelera = 120;
    $frena = 120;
    $carro = new Coche('4080DPM', 60);
    $carro->acelera($acelera);
    echo $carro->__toString().'<br>';
    $carro->frena($frena);
    echo $carro.'<br>';

    require_once 'monedero.php';
    $ingreso = 10;
    $retiro = 10;
    $cuenta = new Monedero(90);

    $cuenta->ingresar($ingreso);
    echo $cuenta->__toString().'<br>';
    $cuenta->retirar($retiro);
    echo $cuenta->__toString().'<br>';


?>