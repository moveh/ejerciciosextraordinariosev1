<?php

    $coches = ['Seat' => ['Altea', 'Leon', 'Cordoba'], 'Audi' => ['A1', 'A2', 'A3'], 'Ferrari' => ['458', 'LaFerrari', 'F40']];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1</title>
</head>
<body>
    <form action="" method="POST">
        <label for="marca">Marca: </label>
        <select name="modelo" id="modelo">
            <?php foreach($coches as $marca => $modelo) :?>
                <option value="<?=$marca?>"><?=$marca?></option>
            <?php endforeach ;?>
        </select>
        <input type="submit" name="mostrar" id="mostrar" value="Mostrar">
    </form>
    <br><br>
    <?php if(isset($_POST['mostrar'])) :?>
        <form action="" method="POST">
            <table border="1">
                <tr>
                    <th><?='Marca: '.$_POST['modelo']?></th>
                </tr>
                <?php foreach($coches[$_POST['modelo']] as $modelo) :?>
                    <tr>
                        <td><input type="text" name="<?=$modelo?>" id="<?=$modelo?>" value="<?=$modelo?>"></td>
                    </tr>
                <?php endforeach ;?>
            </table>
            <br><br>
            <input type="submit" name="actualizar" id="actualizar" value="Actualizar">
        </form>
    <?php endif ;?>
</body>
</html>

<?php
    if(isset($_POST['actualizar'])){
        print_r($_POST);
        foreach($_POST as $key => $value){
            if( && $key != $value){
                echo 'El modelo '.$key.' ha sido actualizado a: '.$value;
            }
        }
    }
?>