<?php
    $peliculas = ['jurassic park', 'titanic', 'king-kong', 'tarzan', 'elysium', 'piratas del caribe', 'los vengadores', 'el diario de noah', 'avatar', 
                    'salvar al soldado ryan'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>3</title>
</head>
<body>
    <form action="" method="POST">
        <input type="search" name="busqueda" id="busqueda">
        <input type="submit" name="buscar" id="buscar" value="Buscar">  
    </form>
</body>
</html>

<?php 
    if(isset($_POST['buscar'])){

        $busqueda = $_POST['busqueda'];
        $resultados = [];
        echo 'buscaste: '.$busqueda.'</br>';

        foreach($peliculas as $pelicula){
            $pos = strpos(strtolower($pelicula), strtolower($busqueda));  
            if($pos === false){
                //NAA
            }else{
                $resultados[] = $pelicula;
            }
        }
        echo 'Estos son los resultados: </br>';
        foreach($resultados as $resultado){
            echo '<br>'.$resultado;
        }
    }
?>