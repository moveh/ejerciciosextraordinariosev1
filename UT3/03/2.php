<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2</title>
</head>
<body>
    <?php
        $dinero = 888.88;
        $billetes = array(500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01);
        echo 'Para la cantidad de '.$dinero.' €, el desglose es: </br>';
        foreach($billetes as $billete){
            if($dinero > 0){
                $cantidad = floor($dinero/$billete);
                $dinero = $dinero%$billete;
                echo $cantidad ." de ". $billete ."</br>";
            }else{
                $dinero-=$cantidad*$billete;
                echo $cantidad ." de ". $billete ."</br>";
            }
        }
    ?>
</body>
</html>