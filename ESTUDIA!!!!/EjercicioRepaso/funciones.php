<?php
require_once 'conexiones.php';
session_start();


//CONSULTA PREPARADA PDO
function acceso($usuario, $contraseña)
{
    $conexion = conexionPdo();
    $sql = "SELECT pass FROM clientes WHERE usuario=?";
    $consulta = $conexion->prepare($sql);
    $consulta->bindParam(1, $usuario);
    $consulta->execute();
    $pass = $consulta->fetch();

    if($pass){
        if ($pass['pass'] == $contraseña) {
            unset($conexion);
            return true;
        } else {
            unset($conexion);
            return false;
        }
    }
}

//CONSULTA PREPARADA PDO
function getUserId($usuario){
    $conexion = conexionPdo();
    $sql = "SELECT * FROM clientes WHERE usuario=?";
    $consulta = $conexion->prepare($sql);
    $consulta->bindParam(1, $usuario);
    $consulta->execute();
    $usuario = $consulta->fetchAll(PDO::FETCH_ASSOC); //Para mas de un resultado. Si quiero solo 1 resultado usa solo fetch(PDO::FETCH_ASSOC)
    return $usuario;
}

//CONSULTA PREPARADA SQLI SIN PARAMETROS
function getViajes()
{
    $conexion = conexionSqli();
    $consulta = $conexion->stmt_init();
    $consulta->prepare('SELECT nombre_viaje, precio FROM viajes');
    $consulta->execute();
    $consulta->bind_result($nombre, $precio);
    while ($consulta->fetch()) {
        $viajes[$nombre] = $precio; 
    }
    $consulta->close();
    return $viajes;
}

function reserva()
{
    $conexion = conexionPdo();
    $todoOk = true; // Definimos una variable para comprobar la ejecución
    $conexion->beginTransaction(); // Iniciamos la transacción
    $sql = 'UPDATE reservas SET campo=1 WHERE condicion';
    if ($conexion->exec($sql) == 0) $todoOk = false; //Si hay error ponemos false
    $sql = 'INSERT INTO reservas (`num_plazas_reseervadas`) VALUES (“valor1”)';
    if ($conexion->exec($sql) == 0) $todoOk = false; //Si hay error ponemos false
    // Si todo fue bien, confirmamos los cambios y en caso contrario los deshacemos
    if ($todoOk == true) {
        $conexion->commit();
        print "<p>Los cambios se han realizado correctamente.</p>";
    } else {
        $conexion->rollback();
        print "<p> No se han podido realizar los cambios. </p>";
    }
}
