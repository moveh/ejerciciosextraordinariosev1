<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $temporal = 'Juan';
        echo ('Juan --> '.gettype($temporal).'<br>');

        $temporal = 3.14;
        echo ('3.14 --> '.gettype($temporal).'<br>');

        $temporal = false;
        echo ('false --> '.gettype($temporal).'<br>');

        $temporal = 3;
        echo ('3 --> '.gettype($temporal).'<br>');

        $temporal = null;
        echo ('null --> '.gettype($temporal).'<br>');
    ?>
</body>
</html>