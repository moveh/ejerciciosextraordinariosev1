<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Doce</title>
</head>
<body>
    <?php

        //Numeros primos entre 3 y 999.
    
        for($numero=3; $numero<=999; $numero++){
            $primo=true;
            for($i=2; $i<=$numero/2 && $primo; $i++){
                if ($numero % $i == 0){
                    $primo=false;
                }
            }
            echo $primo == true ? "$numero Es primo <br/>" : null;
        }
        

    ?>
</body>
</html>