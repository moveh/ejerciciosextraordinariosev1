<?php
    $peliculas = ['jurassic park' => 'jurassic.jpg', 'titanic' => 'titanic.jpg', 'king-kong' => 'kingkong.jpg', 'tarzan' => 'tarzan.jpg', 'elysium' => 'elysium.jpg', 'piratas del caribe' => 'piratas.jpg', 'los vengadores' => 'vengadores.jpg'
                    , 'el diario de noa' => 'noa.jpg', 'avatar' => 'avatar.jpg', 'salvar al soldado ryan' => 'salvar.jpg'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4</title>
</head>
<body>
    <form action="" method="POST">
        <input type="search" name="busqueda" id="busqueda">
        <input type="submit" name="buscar" id="buscar" value="Buscar">  
    </form>
    <?php 
    print_r($peliculas);
    if(isset($_POST['buscar'])){
        $cont = 0;
        $busqueda = $_POST['busqueda'];
        $resultados = [];
        echo 'buscaste: '.$busqueda.'</br>';

        foreach($peliculas as $titulo => $img){
            $pos = strpos(strtolower($titulo), strtolower($busqueda));  
            if($pos === false){
                //NAA
            }else{
                $cont += 1;
                $resultados[] = [$titulo, $img];

            }
        }
        echo 'Se han encontrado un total de '.$cont.' resultados: </br>';
    }
?>
    <table border="1">
        <tr>
            <th>Imagen</th>
            <th>Título</th>
        </tr>
        <?php if(isset($resultados)) :?>
            <?php foreach($resultados as $resultado => $img) :?>
            <tr>
                <td><img src="pelis/<?= $img[1] ?>" alt="nada" width="200px" height="200px"></td>
                <td><?= $img[0] ?></td>
            </tr>
            <?php endforeach ;?>
        <?php endif ;?>
    </table>
</body>
</html>