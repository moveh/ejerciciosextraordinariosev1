<?php
    require_once 'funciones.php';
    $asientos = getAsientosSqli();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reserva</title>
</head>
<body>
    <h1>Reserva de asiento</h1>
    <form action="" method="POST">
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" id="nombre">
        <br><br>
        <label for="dni">DNI:</label>
        <input type="text" name="dni" id="dni">
        <br><br>
        <label for="asiento">Asiento:</label>
        <select name="asiento" id="asiento">
            <?php foreach($asientos as $asiento) :?>
            <?= $asiento ?>
                <option value="<?=$asiento['numero']?><?=$asiento['precio']?>"><?=$asiento['numero']?> (<?=$asiento['precio']?>)</option>
            <?php endforeach ;?>
        </select>
        <br><br>
        <input type="submit" name="reservar" id="reservar" value="Reservar">
    </form>
</body>
</html>