<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

        $var1 = 1;
        $var2 = 2;
        $var3;
        echo 'En un principio la variable uno tiene el valor '.$var1.' y la variable dos el valor '.$var2.'<br>';

        //intercambiamos
        $var3 = $var1;
        $var1 = $var2;
        $var2 = $var3;

        echo 'Finalmente la variable uno tiene el valor '.$var1.' y la variable dos el valor '.$var2;
    ?>
</body>
</html>