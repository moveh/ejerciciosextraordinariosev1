<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1</title>
</head>
<body>
    <?php
        $primero = array();
        $segundo = array();
        $tercero = array();

        for($i = 0; $i < 20; $i++){
            array_push($primero, $i);
        }

        for($n = 20; $n < 40; $n++){
            array_push($segundo, $n);
        }

        $tercero = array_merge($primero, $segundo);

        foreach ($tercero as $valor){
            echo $valor.' ';
        }
    ?>
</body>
</html>