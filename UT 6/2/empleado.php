<?php

    class Empleado{
        protected $nombre;
        protected $dni;
        private $sueldo;

        public function __construct($nombre, $dni, $sueldo)
        {
            $this->nombre = $nombre;
            $this->dni = $dni;
            $this->sueldo = $sueldo;
        }

        public function getSueldo($sueldo)
        {
            return $this->sueldo;
        }

        public function __toString()
        {
            return $this->nombre.' '.$this->dni.' '.$this->sueldo;
        }
    }

    class Encargado extends Empleado{

        private $empleado;
        public function __construct($nombre, $dni, $sueldo, $empleado)
        {
            parent::__construct($nombre, $dni, $sueldo*1.15);
            $this->empleado = $empleado;
        }

        public function __toString()
        {
            return parent::__toString().' '.$this->empleado;
        }
    }

?>