<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5</title>
</head>
<body>
    <?php
        $articulos = array('pegamento' => array('codigo' => 0, 'descripcion' => 'cosa que adhiere', 'existencias' => 10), 
                            'boligrafo' => array('codigo' => 1, 'descripcion' => 'cosa que pinta', 'existencias' => 11),
                            'tijeras' => array('codigo' => 2, 'descripcion' => 'cosa que corta', 'existencias' => 12));
        
                            
        function mayor($articulos){
            $mayor = 0;

            foreach($articulos as $articulo => $nombre){
                if( $nombre['existencias'] > $mayor){
                    $mayor = 'El articulo con más existencias es '.$articulo;
                }
            }

            
            return $mayor;
        }

        function sumar($articulos){
            $suma = 0;

            foreach($articulos as $articulo => $nombre){
                $suma += $nombre['existencias'];
            }

            $muestra = 'Las existencias totales son: '.$suma;

            return $muestra;
        }

        function mostrar($articulos){
            foreach($articulos as $articulo => $nombre){
                echo "<h1> $articulo </h1>";
                foreach($nombre as $indice => $valor){
                    echo "<p> $indice:$valor </p>";
                }
            }
        }

        echo mayor($articulos).'</br>';
        echo sumar($articulos).'</br>';
        echo mostrar($articulos);
    ?>
</body>
</html>