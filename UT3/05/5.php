<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5</title>
</head>
<body>
    <form action="" method="POST">
        <?php for($i = 0; $i < 10; $i++) :?>
            <label for="<?=$i?>"><?='Cantidad '.($i + 1)?></label>
            <input type="number" name="<?=$i?>" id="<?=$i?>" value="<?= $i + 1 ?>">
            <br><br>
        <?php endfor ;?>
        <input type="submit" name="enviar" id="enviar">
    </form>
</body>
</html>

<?php
    if(isset($_POST['enviar'])){
        $numeros = [];
        $suma = 0;

        for($n = 0; $n < 10; $n++){
            $numeros[] = $_POST[$n];
        }

        foreach($numeros as $numero){
            $suma = $suma + $numero;
        }

        echo '<br> La suma total es: '.$suma;
    }
?>