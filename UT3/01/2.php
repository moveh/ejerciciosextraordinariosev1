<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dos</title>
</head>
<body>
    <?php
        $suma = 0;

        for($i = 10; $i < 101; $i+=2){
            $suma += $i;
        }
        
        echo 'La suma total de los números naturales pares entre 10 y 100 es: '.$suma;

    ?>
</body>
</html>