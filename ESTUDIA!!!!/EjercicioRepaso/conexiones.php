<?php

//MYSQLI
function conexionSqli()
{
    $conexion = new mysqli("localhost", "root", "", "ejerciciorepaso", 3306);
    $conexion->set_charset("utf8");
    $error = $conexion->connect_errno;
    if ($error != null) {
        print "<p>Se ha producido el error: $conexion->connect_error.</p>";
        exit();
    } else {
        return $conexion;
    }
}


//PDO
function conexionPdo()
{
    $opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
    $conexion = new PDO('mysql:host=localhost;dbname=ejerciciorepaso', 'root', '', $opciones);
    //unset($conexion);
    return $conexion;
}
