<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ocho</title>
</head>
<body>
    <?php
        $num = 5;
        $res = 1;
        for($i = $num; $i>0; $i--){
            $res*=$i;
        }
        echo 'El factorial de '.$num.' es: '.$res;
    ?>
</body>
</html>