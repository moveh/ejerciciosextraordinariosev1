<?php

class Monedero{
    private $dinero;
    private static $numero_monederos = 0;




    // ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡FUNCION PARA TRABAJAR STATICS INDIVIDUALMENTE!!!!!!!!!!!!!!!!!

    // public static function nuevoMonedero(){
    //     self::$numero_monederos++;
    // }





    //¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡FUNCION PARA BORRAR OBJETOS Y TENER CUIDADO SI HAY STATICS!!!!!!!!!!!!!!!!!!

    // public function __destruct()
    // {
    //     self::$numero_monederos--;
    // }




    public function __construct($dinero){
        $this->dinero = $dinero;
        self::$numero_monederos++;
    }
    
    public function ingresar($ingreso){
        $this->dinero += $ingreso;
    }

    public function retirar($retiro){
        $total = $this->dinero - $retiro;
        if($total < 0){
            return 'No se ha podido retirar dinero';
        }else{
            $this->dinero = $total;
            return 'Se han retirado '.$retiro.' € de la cuenta. Quedan: '.$total.' €';
        }
    }

    public function __toString()
    {
        return 'Dispones de '.$this->dinero.' € de saldo en tu cuenta';
    }

}

?>