<?php
require_once "conexiones.php";

    //TRANSACTION SQLI
    function llegadaSqli(){

        $todoBien = true;
        $conexion = getConexionSqli();
        $conexion -> autocommit(false);
        $preparar = $conexion -> stmt_init();
        $consultaUpdate = "UPDATE plazas SET reservada = 0";
        $preparar -> prepare($consultaUpdate);

        if(!$preparar -> execute()){
            $todoBien = false;
        }
        $preparar -> close();

        $consultaDelete = "DELETE * FROM pasajeros";
        $preparar -> prepare($consultaDelete);

        if(!$preparar -> execute()){
            $todoBien = false;
        }
        $preparar -> close();

        //Si todo fue bien commmit y sino rollback

        if($todoBien){
            $conexion -> commit();
        }else{
            $conexion -> rollback();
        }

        return $todoBien;
    }


    //TRANSACTION PDO
    function llegadaPdo(){
        $conexion = getConexionPdo();
        $todoOkDelete = false;
        $todoOkUpdate = false;
        $todoBien = false;
        $conexion -> beginTransaction();

        $consultaDelete = 'DELETE * FROM pasajeros';
        if($conexion -> exec($consultaDelete) != 0){
            $todoOkDelete = true;
        }

        $consultaUpdate = 'UPDATE plazas SET reservada = 0';
        if($conexion -> exec($consultaUpdate) != 0){
            $todoOkUpdate = true;
        }

        if($todoOkUpdate && $todoOkDelete){
            $todoBien = true;
            $conexion -> commit();
            echo 'Bien';
        }else{
            $conexion -> rollback();
            echo 'Mal';
        }

        unset($conexion);
        return $todoBien;
    }

    function getAsientosSqli(){
        $conexion = getConexionSqli();
        $consulta = "SELECT * FROM plazas WHERE reservada = 0";
        $resultado =  $conexion -> query($consulta);
        if($resultado){
            while($asiento = $resultado -> fetch_array()){
                $asientos[] = array("numero" => $asiento["numero"], "precio" => $asiento["precio"]);
            }
            $resultado -> close();
        }
        $conexion -> close();
        return $asientos;
    }

    function getAsientosPdo(){
        $conexion = getConexionPdo();
        $consulta = "SELECT * FROM plazas WHERE reservada = 0";
        if($resultado = $conexion -> query($consulta)){
            while($asiento = $resultado -> fetch()){
                $asientos[] = array("numero" => $asiento["numero"], "precio" => $asiento["precio"]);
            }
            unset($resultado);
        }
        unset($conexion);
        return $asientos;
    }
?>