<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>6</title>
</head>
<body>
    <?php
        $verbos = array('comer', 'beber', 'abrir', 'vivir', 'saltar', 'ayudar');
        $ar = array('o', 'as', 'a', 'amos', 'ais', 'an');
        $er = array('o', 'es', 'e', 'emos', 'eis', 'en');
        $ir = array('o', 'es', 'e', 'imos', 'is', 'en');
        //Como, bebo, abro, vivo, Salto, ayudo

        $tamanhoArray = count($verbos);
        $num = rand(0, $tamanhoArray-1);
        $verbo = $verbos[$num];

        $verb = substr($verbo, 0, intval($verbo) -2);
        $raiz = substr($verbo, -2);
         
        switch($raiz){
            case 'ar':
                for($i = 0; $i < count($ar); $i++){
                    echo ($verb.$ar[$i].'<br/>');
                }
                break;
            case 'er':
                for($i = 0; $i < count($er); $i++){
                    echo ($verb.$er[$i].'<br/>');
                }
                break;
            case 'ir':
                for($i = 0; $i < count($ir); $i++){
                    echo ($verb.$ir[$i].'<br/>');
                }
                break;
            default:
                echo 'Algo va mal';
                break;
        }

    ?>
</body>
</html>