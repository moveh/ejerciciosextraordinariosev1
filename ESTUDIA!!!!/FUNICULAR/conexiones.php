<?php
function getConexionSqli(){
    $conexion = new mysqli("localhost", "root", "", "funicular", 3306);

        $conexion->set_charset("utf8");

        $error = $conexion->connect_errno;
        
        if ($error != null){
            print "<p>Se ha producido el error: $conexion->connect_error.</p>";
            exit();
        }
        //else { /*hacemos cosas*/ }
        //$conexion->close();
        return $conexion;
}

function getConexionPdo(){
    $opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
        $conexion = new PDO('mysql:host=localhost;dbname=funicular', 'root',
        '' , $opciones);
        return $conexion;
}
?>